import type { Request, Response, NextFunction } from 'express';
import GetMessagesByConversationId from '../services/messages/getMessageByConversationId';
import GetMessagesById from '../services/messages/getMessagesById';
import PostMessage from '../services/messages/postMessage';

class MessagesController {
  async getMessageByConversationId(
    request: Request,
    response: Response,
    next: NextFunction,
  ): Promise<Response | undefined> {
    try {
      const { conversationId } = request.query;
      const id = conversationId?.toString() || '';

      const getMessage = new GetMessagesByConversationId();
      const message = await getMessage.execute({ id });
      return response.json(message);
    } catch (err) {
      console.error(err);
      next(err);
    }
  }
  async getMessageById(request: Request, response: Response, next: NextFunction): Promise<Response | undefined> {
    try {
      const { id } = request.params;
      const getMessage = new GetMessagesById();
      const message = await getMessage.execute({ id });
      return response.json(message);
    } catch (err) {
      console.error(err);
      next(err);
    }
  }
  async post(request: Request, response: Response, next: NextFunction): Promise<Response | undefined> {
    try {
      const { conversationId, from, to, text } = request.body;

      const postMessage = new PostMessage();
      const message = await postMessage.execute({ conversationId, from, to, text });

      return response.json(message);
    } catch (err) {
      console.error(err);
      next(err);
    }
  }
}

export default MessagesController;
