import type { Request, Response, NextFunction } from 'express';
import DeleteBot from '../services/bot/deleteBot';

import GetBotById from '../services/bot/getBotById';
import PostBot from '../services/bot/postBot';
import PutBot from '../services/bot/putBot';

class BotsController {
  async get(request: Request, response: Response, next: NextFunction): Promise<Response | undefined> {
    try {
      const { id } = request.params;
      const getBot = new GetBotById();
      const data = await getBot.execute({ id });
      return response.json(data);
    } catch (err) {
      console.error(err);
      next(err);
    }
  }
  async post(request: Request, response: Response, next: NextFunction): Promise<Response | undefined> {
    try {
      const { name } = request.body;
      const postBot = new PostBot();
      const data = await postBot.execute({ name });
      return response.json(data);
    } catch (err) {
      console.error(err);
      next(err);
    }
  }
  async put(request: Request, response: Response, next: NextFunction): Promise<Response | undefined> {
    try {
      const { id } = request.params;
      const { name } = request.body;
      const putBot = new PutBot();
      const data = await putBot.execute({ id, name });
      return response.json(data);
    } catch (err) {
      console.error(err);
      next(err);
    }
  }
  async delete(request: Request, response: Response, next: NextFunction): Promise<Response | undefined> {
    try {
      const { id } = request.params;
      const deleteBot = new DeleteBot();
      const data = await deleteBot.execute({ id });
      return response.json(data);
    } catch (err) {
      console.error(err);
      next(err);
    }
  }
}

export default BotsController;
