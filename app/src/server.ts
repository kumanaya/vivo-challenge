import type { NextFunction, Request, Response } from 'express';

import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';

import { errors } from 'celebrate';

import AppError from './lib/error';
import pino from './lib/pino';
import logger from './lib/pino';

import router from './routes/routes';

require('dotenv').config();

const HOSTNAME = process.env.HOSTNAME || 'http://localhost';
const PORT = process.env.PORT || 3000;
const DB_MONGO = process.env.DB_MONGO;

mongoose.connect(`${DB_MONGO}`, (error) => {
  if (error) {
    pino.fatal('[MongoDB]: Failed to connected to database');
  }
});

// mongoose.set("debug", true);

const db = mongoose.connection;

db.on('open', () => pino.info("[MongoDB]: we're connected!"));
db.on('error', (error) => pino.fatal(`[MongoDB]: ${error}`));

const app = express();

app.use(express.json());
app.use(errors());
app.use(cors());
app.use(router);

app.use((error: Error, request: Request, response: Response, next: NextFunction) => {
  if (error instanceof AppError) {
    return response.status(error.statusCode).json({
      errorCode: error.errorCode,
      message: error.message,
    });
  }
  return response.status(500).json({
    errorCde: 'UNHANDLED_ERROR',
    message: 'Internal Server Error',
  });
});

app.listen(PORT, () => {
  logger.info(`Server running on ${HOSTNAME}:${PORT}`);
});

export default app;
