import { Router } from 'express';
import { celebrate, Joi, Segments } from 'celebrate';

import BotsController from '../controllers/bots.controller';

const router = Router();

const botsController = new BotsController();

router.get(
  '/bots/:id',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().required(),
    },
  }),
  botsController.get,
);

router.post(
  '/bots',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required(),
    },
  }),
  botsController.post,
);

router.put(
  '/bots/:id',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().required(),
    },
    [Segments.BODY]: {
      name: Joi.string().required(),
    },
  }),
  botsController.put,
);

router.delete(
  '/bots/:id',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().required(),
    },
  }),
  botsController.delete,
);

export default router;
