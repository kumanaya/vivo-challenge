import { Router } from 'express';
import { celebrate, Joi, Segments } from 'celebrate';

import MessagesController from '../controllers/messages.controller';

const router = Router();

const messagesController = new MessagesController();

router.get(
  '/messages/:id',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().required(),
    },
  }),
  messagesController.getMessageById,
);

router.get(
  '/messages',
  celebrate({
    [Segments.QUERY]: {
      conversationId: Joi.string().required(),
    },
  }),
  messagesController.getMessageByConversationId,
);

router.post(
  '/messages',
  celebrate({
    [Segments.BODY]: {
      conversationId: Joi.string().required(),
      from: Joi.string().required(),
      to: Joi.string().required(),
      text: Joi.string().required(),
    },
  }),
  messagesController.post,
);

export default router;
