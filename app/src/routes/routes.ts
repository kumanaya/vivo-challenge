import { Router } from 'express';

import InitialRoutes from '../routes/initial.routes';
import BotsRoutes from '../routes/bot.routes';
import MessagesRoutes from '../routes/messages.routes';

const router = Router();

router.use(InitialRoutes);
router.use(BotsRoutes);
router.use(MessagesRoutes);

export default router;
