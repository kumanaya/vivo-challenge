import BotModel from '../../models/bot.model';
import AppError from '../../lib/error';
import GetBotById from './getBotById';
import GetBotByName from './getBotByName';

interface IRequest {
  name: string;
  id: string;
}

interface IResponse {
  id: string;
  name: string;
  createdAt: Date;
  updatedAt: Date;
}

class PutBot {
  public async execute({ id, name }: IRequest): Promise<IResponse> {
    const getBotByName = new GetBotByName();
    const getBotById = new GetBotById();

    const botAlreadyExistsByName = await getBotByName.execute({ name });

    if (Object.keys(botAlreadyExistsByName).length >= 1) {
      throw new AppError(400, 'BOT_ALREADY_EXISTS', 'Bot already exists on database. Try to pick different name');
    }

    const botAlreadyExistsById = await getBotById.execute({ id });

    if (Object.keys(botAlreadyExistsById).length === 0) {
      throw new AppError(400, 'BOT_DOESNT_EXISTS', 'Bot doesnt exists on database. Try to pick a different bot.');
    }

    const bot: any = await BotModel.updateOne({ id }, { name });

    return bot;
  }
}

export default PutBot;
