import BotModel from '../../models/bot.model';
import AppError from '../../lib/error';
import GetBotById from './getBotById';

interface IRequest {
  id: string;
}

interface IResponse {
  statusCode: string;
  message: string;
}

interface IBot {
  id: string;
  name: string;
}

class DeleteBot {
  async execute({ id }: IRequest): Promise<IResponse> {
    const getBotById = new GetBotById();
    const botAlreadyExistsById = await getBotById.execute({ id });
    if (Object.keys(botAlreadyExistsById).length === 0) {
      throw new AppError(400, 'BOT_DOESNT_EXISTS', 'Bot doesnt exists on database. Try to pick a different bot.');
    }

    const bot: any = await BotModel.deleteOne({ id });

    return {
      statusCode: 'SUCCESS',
      message: `Success to delete: ${bot.name}`,
    };
  }
}

export default DeleteBot;
