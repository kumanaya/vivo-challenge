import BotModel from '../../models/bot.model';
import GetBotByName from './getBotByName';
import AppError from '../../lib/error';

interface IRequest {
  name: string;
}

interface IResponse {
  name: string;
}

class PostBot {
  public async execute({ name }: IRequest): Promise<IResponse> {
    const getBot = new GetBotByName();
    const bots = await getBot.execute({ name });
    if (Object.keys(bots).length >= 1) {
      throw new AppError(400, 'BOT_ALREADY_EXISTS', 'Bot already exists on database. Try a different name');
    }

    const bot = await BotModel.create({
      name,
    });

    return bot;
  }
}

export default PostBot;
