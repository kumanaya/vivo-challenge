import BotModel from '../../models/bot.model';

interface IRequest {
  id: string;
}

interface IResponse {
  id: string;
  name: string;
  createdAt: Date;
  updatedAt: Date;
}

class GetBotById {
  async execute({ id }: IRequest): Promise<IResponse | {}> {
    const bot = await BotModel.findById(id);
    return bot != null ? bot : {};
  }
}

export default GetBotById;
