import BotModel from '../../models/bot.model';

interface IRequest {
  name: string;
}

interface IResponse {
  id: string;
  name: string;
  createdAt: Date;
  updatedAt: Date;
}

class GetBotByName {
  async execute({ name }: IRequest): Promise<IResponse | {}> {
    const bot = await BotModel.find({ name: name });
    return bot != null ? bot : {};
  }
}

export default GetBotByName;
