import MessageModel from '../../models/message.model';

interface IRequest {
  id: string;
}

interface IResponse {
  id: string;
  conversationId: string;
  from: string;
  to: string;
  text: string;
  timestamp: Date;
  createdAt: Date;
  updatedAt: Date;
  toBot: {
    name: string;
    updatedAt: Date;
    createdAt: Date;
  };
}

class GetMessagesById {
  async execute({ id }: IRequest): Promise<IResponse | {}> {
    const bot = await MessageModel.findById(id);
    return bot != null ? bot : {};
  }
}

export default GetMessagesById;
