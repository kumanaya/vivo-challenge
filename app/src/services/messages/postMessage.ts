import { ObjectID } from 'bson';

import MessageModel from '../../models/message.model';
import AppError from '../../lib/error';
import GetBotById from '../bot/getBotById';

interface IRequest {
  conversationId: string;
  from: string;
  to: string;
  text: string;
}

interface IResponse {
  id: string;
  conversationId: string;
  from: string;
  to: string;
  text: string;
  timestamp: Date;
  createdAt: Date;
  updatedAt: Date;
}

class PostMessage {
  async execute({ conversationId, from, to, text }: IRequest): Promise<IResponse> {
    const getBot = new GetBotById();
    const botExists: any = await getBot.execute({ id: conversationId });
    if (Object.keys(botExists).length === 0) {
      throw new AppError(400, 'BOT_DOESNT_EXISTS', 'Bot doesnt exists in database. Try to pick a different one.');
    }

    const message: any = await MessageModel.create({
      conversationId,
      from: new ObjectID(from),
      to: new ObjectID(to),
      text,
    });

    return message;
  }
}

export default PostMessage;
