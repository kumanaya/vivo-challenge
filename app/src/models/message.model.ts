import { Schema, model } from 'mongoose';
import { ObjectID } from "bson";

interface IMessage {
  conversationId: string;
  timestamp: Date;
  from: string;
  to: string;
  text: string;
}

const MessageSchema = new Schema<IMessage>({
  conversationId: String,
  timestamp: String,
  from: ObjectID,
  to: ObjectID,
  text: String,
});

const MessageModel = model<IMessage>('Message', MessageSchema);

export default MessageModel;
