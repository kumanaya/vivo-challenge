import { Schema, model } from 'mongoose';

interface IBot {
  name: string;
}

const BotSchema = new Schema<IBot>({
  name: String,
});

const BotModel = model<IBot>('Bot', BotSchema);

export default BotModel;
